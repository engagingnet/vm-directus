module.exports = {
  apps : [
    {
      name: "directus-vm",
      script: "npx",
      args: "directus start",
      restart_delay: 500
    }
  ]
}